# !usr/bin/Python3.6
import numpy as np

"""
Quick sort algorithm
This divide and conquer algorithm is the most often used sorting algorithm. When configured correctly, it's extremely
efficient and does not require the extra space Merge sort uses. We partition the list around a pivot element, sorting
values around the pivot.

The merge sort time complexity is O(nlog(n))

------------------------------------------------------------------------------------------------------------------------

Quick sort begins by partitioning the list - picking one value of the list that will be in its sorted place. This value
is called pivot. All elements smaller than the pivot are moved to its lef. All larger elements are moved to its right.

Knowing that the pivot is in it's rightful place, we recursively sort the values around the pivot until the entire list
is sorted.
"""


def partition(nums, low, high):
    # We select the middle element to be the pivot. Some implementations select the first element or the last element.
    # Somentimes the media value becomes the pivot, or a random one. There are many more strategies that can be choosen
    # or created.
    pivot = nums[(low + high) // 2]
    i = low - 1
    j = high + 1
    while True:
        i += 1
        while nums[i] < pivot:
            i += 1
        j -= 1
        while nums[j] > pivot:
            j -= 1
        if i >= j:
            return j
        nums[i], nums[j] = nums[j], nums[i]


def quick_sort(nums):
    def _quick_sort(items, low, high):
        if low < high:
            split_index = partition(items, low, high)
            _quick_sort(items, low, split_index)
            _quick_sort(items, split_index + 1, high)
    _quick_sort(nums, 0, len(nums) - 1)


r_l = np.random.randint(0, 100, 100)
quick_sort(r_l)
print(r_l)
