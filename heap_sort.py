# !usr/bin/Python3.6
import numpy as np

"""
Heap sort algorithm
Like the insertion and slection sorts, segments the list into sorted and unsorted parts. It converts the unsorted
segment of the list to a Heap data structure, so that we can efficiently determine the largest element.

The insertion sort time complexity is O(nlog(n))

------------------------------------------------------------------------------------------------------------------------

We begin by transforming the list into a Max Heap - a Binary Tree where the biggest element is the root node. We then
place that item to the end of the list. We then rebuild our Max Heap which now has one less value, placing the new 
largest value before the last item of the list.

We iterate this process of buiding the heap until all nodes are removed.
"""


def heapify(nums, heap_size, root_index):
    largest = root_index
    left_child = (2 * root_index) + 1
    right_child = (2 * root_index) + 2

    # If the left index of the root is a valid index, and the element is greater than the current largest element,
    # then update the largest element.
    if left_child < heap_size and nums[left_child] > nums[largest]:
        largest = left_child

    # Do the same for the right child of the root
    if right_child < heap_size and nums[right_child] > nums[largest]:
        largest = right_child

    # If the largest element is no longer the root element, swap them
    if largest != root_index:
        nums[root_index], nums[largest] = nums[largest], nums[root_index]
        heapify(nums, heap_size, largest)


def heap_sort(nums):
    n = len(nums)
    for i in range(n, -1, -1):
        heapify(nums, n, i)

    for i in range(n -1, 0, -1):
        nums[i], nums[0] = nums[0], nums[i]
        heapify(nums, i, 0)


r_l = np.random.randint(0, 100, 100)
heap_sort(r_l)
print(r_l)
