# !usr/bin/Python3.6
import numpy as np

"""
Insertion sort algorithm
Like selection sort, this algorithm segments the list into sorted and unsorted parts. It iterates over the unsorted
segment, and inserts the element being viewed into the correcto position of the sorted list.

The insertion sort time complexity is O(n^2)

------------------------------------------------------------------------------------------------------------------------

We assume that the first element of the list is sorted. We then go to the next element, let's call it x. If x is larger
than the first element we leave as is. If x is smaller, we copy the value of the first element to the second position
and then set the first element to x.

As we go to the other elements of the unsorted segment, we continuosly move larger elements in the sorted segmenet up
the list until we encounter an element smaller than x or reach the end of the sorted segment and then place x in it's
correct position.
"""


def insertion_sort(nums):
    for i in range(1, len(nums)):
        item_to_insert = nums[i]
        j = i - 1
        while j >= 0 and nums[j] > item_to_insert:
            nums[j + 1] = nums[j]
            j -= 1
        nums[j + 1] = item_to_insert


r_l = np.random.randint(0, 100, 100)
insertion_sort(r_l)
print(r_l)
