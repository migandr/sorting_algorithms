# !usr/bin/Python3.6
import numpy as np


"""
Bubble Sort algorithm:
The simple sorting algorithm iterates over a list, compairing elements in pairs and swapping them until the larger
elements "bubble up" to the end of the list, and the smaller elements stays at the "bottom".

The bubble sort time complexity is O(n^2)

------------------------------------------------------------------------------------------------------------------------

Compare the first two elements of the list. If the first element is larger than the second element, we swap them.
If they are already in order we leave them as is. We then move to the next pair of elements, compare their values and
swap as necessary.

To optimize the algorithm we need to stop it when it's finished sorting, otherwise, it'll reevalute an already sorted
array many times.

The algorythm runs in a while loop, only breaking when no items are swapped. We set a swapped to True in the beginning
to ensure that the algorithm runs at least once.
"""


def bubble_sort(nums):
    swapped = True
    while swapped:
        swapped = False
        for i in range(len(nums) - 1):
            if nums[i] > nums[i + 1]:
                nums[i], nums[i + 1] = nums[i + 1], nums[i]
                swapped = True


r_l = np.random.randint(0, 100, 100)
bubble_sort(r_l)
print(r_l)

