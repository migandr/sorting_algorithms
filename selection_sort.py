# !usr/bin/Python3.6
import numpy as np

"""
Selection sort algorithm
This algorithm segments the list into two parts: sorted and unsorted. We continuosly remove the smallest element of the
unsorted segment of the list and append it to the sorted segment

The selection sort time complexity is O(n^2)

------------------------------------------------------------------------------------------------------------------------

In practice, we don't need to create a new list for the sorted elements, what we do is treat the leftmost part of the
list as the sorted segment. We then search the entire list for the smalest element and swap it with the first element.

Now we know that the first element of the list is sorted, we get the smallest element of the remaining items and swap it
with the second element. This reiterates until the last item of the list is the remaining element to be examined.
"""


def selection_sort(nums):
    for i in range(len(nums)):
        lowest_value_index = i
        for j in range(i + 1, len(nums)):
            if nums[j] < nums[lowest_value_index]:
                lowest_value_index = j
        nums[i], nums[lowest_value_index] = nums[lowest_value_index], nums[i]


r_l = np.random.randint(0, 100, 100)
selection_sort(r_l)
print(r_l)
