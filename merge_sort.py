# !usr/bin/Python3.6
import numpy as np

"""
Merge sort algorithm
The divide and conquer algorithm splits list in half, and keeps splitting the list by 2 until it only has singular
elements

Adjacent elements become sorted pairs, then sorted pairs are merged and sorted with other pairs as well. This process
continues until we get a sorted list with all the elements of the unsorted list.

The merge sort time complexity is O(nlog(n))

------------------------------------------------------------------------------------------------------------------------

We recursively split the list in half until we have lists with size one. We then merge each half that was split, sorting
them in the process.

Sorting is done by comparing the smallest element of each half. The first element of each list are the first to be
compared. If the first half begin with a smaller value, then we add that to the sorted list. We then compare the second
smallest value of the first half with the first smalles value of the second half.

Every time we select the smaller value at the beginning of a half, we move the index of which item needs to be compared
by one.
"""


def merge(left_list, right_list):
    sorted_list = []
    left_list_index = right_list_index = 0

    left_list_length, right_list_length = len(left_list), len(right_list)
    for _ in range(left_list_length + right_list_length):
        if left_list_index < left_list_length and right_list_index < right_list_length:
            # We check which value from the start of each list is smaller
            # If the item at the beginning of the left list is smaller, add it to the sorted list
            if left_list[left_list_index] <= right_list[right_list_index]:
                sorted_list.append(left_list[left_list_index])
                left_list_index += 1
            else:
                # If the item a the beginning of the right list is smaller, add it to the sorted list
                sorted_list.append(right_list[right_list_index])
                right_list_index += 1
        elif left_list_index == left_list_length:
            # If we've reached the end of the left list, add the elements from the right list
            sorted_list.append(right_list[right_list_index])
            right_list_index += 1
        elif right_list_index == right_list_length:
            # If we've reached the end of the right list, add the elements from the left list
            sorted_list.append(left_list[left_list_index])
            left_list_index += 1
    return sorted_list


def merge_sort(nums):
    # if the list is a single element, return it
    if len(nums) <= 1:
        return nums
    mid = len(nums) // 2
    left_list = merge_sort(nums[:mid])
    right_list = merge_sort(nums[mid:])
    return merge(left_list, right_list)


r_l = np.random.randint(0, 100, 100)
print(merge_sort(r_l))
